import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.core.window import Window

# Set the app size
Window.size = (700, 700)

# Designate the .kv design file
# Builder.load_file('calculator.kv')


class MyLayout(Widget):
    def clear(self):
        """
        Clear the Calculator text
        """
        self.ids.calc_input.text = '0'

    def button_press(self, button):
        """
        Button pressing function
        """
        # Create a variable that contains whatever was already in the textbox
        prior = self.ids.calc_input.text

        # Test for error
        if "Error" in prior:
            prior = ''

        # determine if 0 is sitting there
        if prior == "0":
            self.ids.calc_input.text = ''
            self.ids.calc_input.text = f'{button}'
        else:
            self.ids.calc_input.text = f'{prior}{button}'

    def dot(self):
        """
        Add decimal separator
        """
        prior = self.ids.calc_input.text
        # Split our textbox by +
        num_list = prior.split('+')
        if '+' in prior and '.' not in num_list[-1]:
            # Add a decimal to the end of the text
            prior = f'{prior}.'
            # Output back to the textbox
            self.ids.calc_input.text = prior
        elif '.' in prior:
            pass
        else:
            # Add a decimal to the end of the text
            prior = f'{prior}.'
            # Output back to the textbox
            self.ids.calc_input.text = prior

    def backspace(self):
        """
        Remove last character from textbox
        """
        prior = self.ids.calc_input.text
        # Remove the last item in the textbox
        prior = prior[:-1]
        # Output back to the textbox
        self.ids.calc_input.text = prior

    def post_neg(self):
        """
        Make text in textbox negative or positive
        """
        prior = self.ids.calc_input.text
        # Test to see if there is a - sign already
        if '-' in prior:
            self.ids.calc_input.text = f'{prior.replace("-", "")}'
        else:
            self.ids.calc_input.text = f'-{prior}'

    def math_sign(self, sign):
        """
        Add a math operator to the output box function
        sign: string Mazth operator: +, -, *, /
        """
        prior = self.ids.calc_input.text
        # slap a plus sign to the textbox
        self.ids.calc_input.text = f'{prior}{sign}'

    def equals(self):
        """
        Compute result and output it in the textbox
        """
        prior = self.ids.calc_input.text

        # Replace special math characters with computer math operator
        prior = prior.replace('÷', '/')
        prior = prior.replace('x', '*')

        # Error handling
        try:
            # Evaluate the math from the textbox
            answer = eval(prior)
        except Exception as e:
            answer = "Error"
            print(str(e))
        # Output the answer
        self.ids.calc_input.text = str(answer)


class CalculatorApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    CalculatorApp().run()
