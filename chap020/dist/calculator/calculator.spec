# -*- mode: python ; coding: utf-8 -*-
from kivy_deps import sdl2, glew

block_cipher = None


a = Analysis(['calculator.py'],
             pathex=['L:\\Programs\\laragon_full\\www\\Labs\\Codemy.com\\Kivy\\chap020'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
a.datas+=[('Code\\calculator.kv', 'L:\\Programs\\laragon_full\\www\\Labs\\Codemy.com\\Kivy\\chap020\calculator.kv', 'DATA')]
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='calculator',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=False )
coll = COLLECT(exe,
            Tree('L:\\Programs\\laragon_full\\www\\Labs\\Codemy.com\\Kivy\\chap020'),
               a.binaries,
               a.zipfiles,
               a.datas,
               *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
               strip=False,
               upx=True,
               upx_exclude=[],
               name='calculator')
