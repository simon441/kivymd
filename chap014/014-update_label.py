import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder

# Designate the .kv design file
Builder.load_file('update_label.kv')


class MyLayout(Widget):
    def press(self):
        # variables for the widgets
        name = self.ids.name_input.text

        # Update the label
        self.ids.name_label.text = f'Hello {name}!'

        # Clear input box
        self.ids.name_input.text = ''


class AwesomeApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    AwesomeApp().run()
