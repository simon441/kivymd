from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.uix.tabbedpanel import TabbedPanel

# Designate the .kv design file
Builder.load_file('tabs.kv')


class MyLayout(TabbedPanel):
    pass


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
