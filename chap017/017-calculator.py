import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.core.window import Window

# Set the app size
Window.size = (500, 700)

# Designate the .kv design file
# Builder.load_file('calculator.kv')


class MyLayout(Widget):
    def clear(self):
        """
        Clear the Calculator text
        """
        self.ids.calc_input.text = '0'

    def button_press(self, button):
        """
        Button pressing function
        """
        # Create a variable that contains whatever was already in the textbox
        prior = self.ids.calc_input.text

        # determine if 0 is sitting there
        if prior == "0":
            self.ids.calc_input.text = ''
            self.ids.calc_input.text = f'{button}'
        else:
            self.ids.calc_input.text = f'{prior}{button}'

    def dot(self):
        """
        Add decimal separator
        """
        prior = self.ids.calc_input.text
        if '.' in prior:
            pass
        else:
            # Add a decimal to the end of the text
            prior = f'{prior}.'
            # Output back to the textbox
            self.ids.calc_input.text = prior

    def backspace(self):
        """
        Remove last character from textbox
        """
        prior = self.ids.calc_input.text
        # Remove the last item in the textbox
        prior = prior[:-1]
        # Output back to the textbox
        self.ids.calc_input.text = prior

    def post_neg(self):
        """
        Make text in textbox negative or positive
        """
        prior = self.ids.calc_input.text
        # Test to see if there is a - sign already
        if '-' in prior:
            self.ids.calc_input.text = f'{prior.replace("-", "")}'
        else:
            self.ids.calc_input.text = f'-{prior}'

    def math_sign(self, sign):
        """
        Add a math operator to the output box function
        sign: string Mazth operator: +, -, *, /
        """
        prior = self.ids.calc_input.text
        # slap a plus sign to the textbox
        self.ids.calc_input.text = f'{prior}{sign}'

    def equals(self):
        """
        Compute result
        """
        prior = self.ids.calc_input.text

        # Addition
        if '+' in prior:
            num_list = prior.split('+')
            answer = 0.0
            for number in num_list:
                answer += float(number)
            # print the answer in the textbox
            self.ids.calc_input.text = str(answer)
        # Substraction
        elif '-' in prior:
            num_list = prior.split('-')
            answer = 0
            for number in num_list:
                answer -= float(number)
            # print the answer in the textbox
            self.ids.calc_input.text = str(answer)
        # Substraction
        elif 'x' in prior:
            num_list = prior.split('x')
            answer = 1
            for number in num_list:
                answer *= float(number)
            # print the answer in the textbox
            self.ids.calc_input.text = str(answer)
        # Substraction
        # elif '÷' in prior:
        #     num_list = prior.split('÷')
        #     answer = 0
        #     try:
        #         for number in num_list:
        #             answer /= float(number)

        #     except ZeroDivisionError:
        #         answer = "Division by Zero"
        #     # print the answer in the textbox
        #     self.ids.calc_input.text = str(answer)


class CalculatorApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    CalculatorApp().run()
