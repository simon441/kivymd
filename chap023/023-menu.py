import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.core.window import Window


# Designate the .kv design file
Builder.load_file('menu.kv')


class MyLayout(Widget):
    def selected(self, filename):
        """
        on file selection
        """
        try:
            self.ids.my_image.source = filename[0]
        except:
            pass


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
