from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder

# Designate the .kv design file
Builder.load_file('button_image.kv')


class MyLayout(Widget):
    def hello_on(self):
        self.ids.my_label.text = 'You pressed the button!'
        self.ids.my_image.source = 'images/login_pressed.png'

    def hello_off(self):
        self.ids.my_label.text = 'You released the button!'
        self.ids.my_image.source = 'images/login.png'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
