from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder

# Designate the .kv design file
Builder.load_file('spin.kv')


class MyLayout(Widget):
    def spinner_click(self, value: str):
        self.ids.click_label.text = f'You selected: {value}'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
