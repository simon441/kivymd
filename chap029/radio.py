import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder

# Designate the .kv design file
Builder.load_file('radio.kv')


class MyLayout(Widget):
    selected_toppings = []

    def checkbox_click(self, instance, value, topping):
        # print(instance, value)
        self.ids.output.text = str(value)
        if value == True:
            self.selected_toppings.append(topping)
        else:
            self.selected_toppings.remove(topping)
        selected = " ".join(self.selected_toppings)
        if len(self.selected_toppings) > 0:
            self.ids.output.text = f'You selected: {selected}'
        else:
            self.ids.output.text = f'No topping selected'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
