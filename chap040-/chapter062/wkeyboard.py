from typing import List
from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.label import Label
from kivy.uix.gridlayout import GridLayout
from kivy.uix.vkeyboard import VKeyboard


class MainApp(MDApp):

    DEFAULT_TEXT_LABEL = 'Type Something!'

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Define the layout
        layout = GridLayout(cols=1)
        # Define the label
        self.label = Label(text=self.DEFAULT_TEXT_LABEL, font_size=20)
        # Define the VKeyboard
        keyboard = VKeyboard(on_key_up=self.key_up)
        keyboard.layout = 'azerty'

        # populate layout
        layout.add_widget(self.label)
        layout.add_widget(keyboard)

        return layout

    def key_up(self, keyboard: VKeyboard, keycode, *args: list):
        if isinstance(keycode, tuple):
            keycode = keycode[1]

        # Tracking what was already in the label
        old_content = self.label.text

        # Actions on keyboard key name
        if old_content == self.DEFAULT_TEXT_LABEL:
            old_content = ''
        # Backspace
        if keycode == 'backspace':
            old_content = old_content[:-1]
            keycode = ''
        # Spacebar
        if keycode == 'spacebar':
            keycode = ' '

        # Update the label
        self.label.text = f'{old_content}{keycode}'


if __name__ == '__main__':
    MainApp().run()
