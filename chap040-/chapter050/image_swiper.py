from kivy.lang import Builder
from kivymd.app import MDApp

# Image swiper


class MainApp(MDApp):

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('image_swiper.kv')

    def on_swipe_right(self):
        """ Swipe right """
        # print('You swiped right!')
        self.root.ids.toolbar.title = 'You Swiped Right!'

    def on_swipe_left(self):
        """ Swipe left """
        # print('You swiped left!')
        self.root.ids.toolbar.title = 'You Swiped Left!'


MainApp().run()
