# KivyMD virtual envs

**!!unneeded!!**

Also you can install manually from sources. Just clone the project and run pip:

```bash
git clone https://github.com/kivymd/KivyMD.git --depth 1
# to get the commit for the version i this repo:
# git checkout d93a78c33dd2de8bcbd5a3627858721cff443a77
cd KivyMD
pip install .
cd ..
pip freeze > requirements.txt

update git repo:
git pull
```

## what was just done

installed development version from master branch, you should specify link to zip archive:

`pip install https://github.com/kivymd/KivyMD/archive/master.zip`
