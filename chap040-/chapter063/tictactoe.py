from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.button import Button


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Define whose turn it is
        self.turn = 'X'

        return Builder.load_file('tictactoe.kv')

    def presser(self, button: Button):
        """Action on the press of one of the game board button"""
        if self.turn == 'X':
            button.text = 'X'
            button.disabled = True
            self.root.ids.score.text = "O's Turn!"
            self.turn = 'O'
        else:
            button.text = 'O'
            button.disabled = True
            self.root.ids.score.text = "X's Turn!"
            self.turn = 'X'

    def restart(self):
        """Restart the game"""
        # Reset who's turn it is
        self.turn = 'X'

        # Enable the buttons
        self.root.ids.button1.disabled = False
        self.root.ids.button2.disabled = False
        self.root.ids.button3.disabled = False
        self.root.ids.button4.disabled = False
        self.root.ids.button5.disabled = False
        self.root.ids.button6.disabled = False
        self.root.ids.button7.disabled = False
        self.root.ids.button8.disabled = False
        self.root.ids['button9'].disabled = False

        # Clear the buttons
        self.root.ids.button1.text = ''
        self.root.ids.button2.text = ''
        self.root.ids.button3.text = ''
        self.root.ids.button4.text = ''
        self.root.ids.button5.text = ''
        self.root.ids.button6.text = ''
        self.root.ids.button7.text = ''
        self.root.ids.button8.text = ''
        self.root.ids['button9'].text = ''


if __name__ == '__main__':
    MainApp().run()
