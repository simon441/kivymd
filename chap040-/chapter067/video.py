from kivymd.app import MDApp
from kivy.uix.videoplayer import VideoPlayer


class MainApp(MDApp):
    title = 'Simple Video'

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Create VideoPlayer Instance
        player = VideoPlayer(source='videos/Toki wo Koete.webm')

        # Assign VideoPlayer State
        player.state = 'play'

        # Set options
        player.options = {'eos': 'stop'}  # eos: stop, loop

        # Allow stretch
        player.allow_stretch = True
        # Allow fullscreen
        player.allow_fullscreen = True

        # Return the player
        return player


MainApp().run()
