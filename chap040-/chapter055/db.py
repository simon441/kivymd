from kivy.lang import Builder
from kivymd.app import MDApp
import sqlite3


DATABASE_NAME = 'first_fb.sqlite3'

# Sqlite Database Management


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Create Database or Connect to One if it exists
        conn = sqlite3.connect(DATABASE_NAME)

        # Create a Cursor
        cur = conn.cursor()

        # Create a Table
        cur.execute("""
        CREATE TABLE IF NOT EXISTS customers (
            name TEXT
            )
        """)

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

        return Builder.load_file('first_db.kv')

    def submit(self):
        """ Insert data into database """

        # Create Database or Connect to One if it exists
        conn = sqlite3.connect(DATABASE_NAME)

        # Create a Cursor
        cur = conn.cursor()

        # Add a record
        cur.execute("INSERT INTO customers (name) VALUES( :first)", {
            'first': self.root.ids.word_input.text.strip(),
        })

        # Add a message
        self.root.ids.word_label.text = f'{self.root.ids.word_input.text.strip()} Added'

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

    def show_records(self):
        """ Show database records """
        # Create Database or Connect to One if it exists
        conn = sqlite3.connect(DATABASE_NAME)

        # Create a Cursor
        cur = conn.cursor()

        # Grab records from database
        cur.execute("SELECT * FROM customers;")
        records = cur.fetchall()

        word = ''
        # Loop through records
        for record in records:
            word = f'{word} \n{record[0]}'

        # Show database data
        self.root.ids.word_label.text = word

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()


MainApp().run()
