from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.screen import Screen
from kivymd.uix.datatables import MDDataTable
from kivy.metrics import dp  # Convert from Density Pixels to Pixels

# Data Tables


class MainApp(MDApp):

    def build(self):
        # Defin Screen
        screen = Screen()

        # Define a Table
        # docs: https://kivymd.readthedocs.io/en/0.104.1/components/datatables/index.html
        table = MDDataTable(
            pos_hint={'center_x': .5, 'center_y': .5},
            size_hint=(0.9, 0.6),
            check=True,
            use_pagination=True,  # Use page pagination for table or not
            rows_num=3,  # The number of rows displayed on one page of the table
            # Menu height for selecting the number of displayed rows. (in dp)
            pagination_menu_height='240dp',
            # Menu position for selecting the number of displayed rows. ('center' or 'auto')
            pagination_menu_pos='center',
            # Background color in the format (r, g, b, a) (does nothing for now)
            background_color=[1, 0, 0, 1],

            column_data=[
                ('First Name', dp(30)),
                ('Last Name', dp(30)),
                ('Email Address', dp(30)),
                ('Phone Number', dp(30)),
            ],
            row_data=[
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John',     'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John',  'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
            ]
        )

        # Bind the table
        table.bind(on_check_press=self.checked)
        table.bind(on_row_press=self.row_checked)

        self.theme_cls.theme_style = 'Light'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        # return Builder.load_file('datatable.kv')

        # Add table widget to screen
        screen.add_widget(table)

        return screen

    def checked(self, instance_table, current_row):
        """ Function for checkbox pressed """
        print(instance_table, current_row)

    def row_checked(self, instance_table, instance_row):
        """ Function for row pressed """
        print(instance_table, instance_row)


MainApp().run()
