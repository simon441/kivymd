from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.screen import Screen
from kivymd.uix.datatables import MDDataTable
from kivy.metrics import dp  # Convert from Density Pixels to Pixels

# Data Tables


class MainApp(MDApp):

    def build(self):
        # Defin Screen
        screen = Screen()

        # Define a Table
        table = MDDataTable(
            pos_hint={'center_x': .5, 'center_y': .5},
            size_hint=(0.9, 0.6),
            check=True,
            column_data=[
                ('First Name', dp(30)),
                ('Last Name', dp(30)),
                ('Email Address', dp(30)),
                ('Phone Number', dp(30)),
            ],
            row_data=[
                ('John', 'Doe', 'john@doe.com', '(123) 456-7891'),
                ('Mary', 'Smith', 'mary@smith.com', '(123) 456-1112'),
                ('Amelia', 'Smith', 'amelia@smith.com', '(123) 456-7451'),
                ('John', 'Smith', 'john@smith.com', '(123) 456-1234'),
            ]
        )

        # Bind the table
        table.bind(on_check_press=self.checked)
        table.bind(on_row_press=self.row_checked)

        self.theme_cls.theme_style = 'Light'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        # return Builder.load_file('datatable.kv')

        # Add table widget to screen
        screen.add_widget(table)

        return screen

    def checked(self, instance_table, current_row):
        """ Function for checkbox pressed """
        print(instance_table, current_row)

    def row_checked(self, instance_table, instance_row):
        """ Function for row pressed """
        print(instance_table, instance_row)


MainApp().run()
