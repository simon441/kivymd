from kivy.lang import Builder
from kivymd.app import MDApp

from dotenv import dotenv_values
import psycopg2

DATABASE_NAME = 'py_cloud_app'

# Sqlite Database Management

config = dotenv_values('.env')


def heroku_conn():
    return psycopg2.connect(
        host="ec2-18-211-41-246.compute-1.amazonaws.com",
        database="dalilo11l7p24t",
        user="ikyumnardmfjok",
        password="52d3b09a2480693c2649d8c8a6b10514ec9798d08126844f16a06342480e215b",
        port="5432",
    )


def local_conn():
    return psycopg2.connect(
        host='localhost',
        user=config.get('POSTGRES_LOCAL_USER'),
        passwd=config.get('POSTGRES_LOCAL_PASSWORD'),
        database=DATABASE_NAME,

        port="5432",
    )


def get_connection():
    return local_conn()


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = get_connection()

        # Create a Cursor
        cur = conn.cursor()

        # Create a Table
        cur.execute("""
        CREATE TABLE IF NOT EXISTS customers (
            first_name TEXT,
            last_name TEXT
            )
        """)

        # # Check for table existance
        # cur.execute('SELECT * FROM customers')
        # print(cur.description)

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

        return Builder.load_file('postgres_db.kv')

    def submit(self):
        """ Insert data into database """

        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = get_connection()

        # Create a Cursor
        cur = conn.cursor()

        # Add a record
        sql_command = "INSERT INTO customers (first_name) VALUES(%s)"
        values = (self.root.ids.word_input.text.strip(),)

        # Execute SQL Comand
        cur.execute(sql_command, values)

        # Add a message
        self.root.ids.word_label.text = f'{self.root.ids.word_input.text.strip()} Added'

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

    def show_records(self):
        """ Show database records """
        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = get_connection()

        # Create a Cursor
        cur = conn.cursor()

        # Grab records from database
        cur.execute("SELECT * FROM customers;")
        records = cur.fetchall()

        word = ''
        # Loop through records
        for record in records:
            word = f'{word} \n{record[0]}'

        # Show database data
        self.root.ids.word_label.text = word

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()


MainApp().run()
