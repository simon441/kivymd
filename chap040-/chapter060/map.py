from kivymd.app import MDApp
from kivy_garden.mapview import MapView

# Create MapView without kivy file

class MapViewApp(MDApp):
    def build(self):
        # Create MapView
        mapview = MapView(zoom=10, lat=45.8, lon=1.25)
        return mapview

MapViewApp().run()
