from kivymd.app import MDApp
from kivy.lang import Builder

# Create MapView with kivy file

class MapViewApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'
        return Builder.load_file('map2.kv')

MapViewApp().run()
