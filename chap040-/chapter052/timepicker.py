from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.picker import MDTimePicker

# Time picker


class MainApp(MDApp):

    def build(self):
        self.theme_cls.theme_style = 'Light'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('timepicker.kv')

    def show_time_picker(self):
        from datetime import datetime

        # Define default time
        default_time = datetime.strptime('4:20:00', '%H:%M:%S').time()
        time_dialog = MDTimePicker()
        # Set default time
        time_dialog.set_time(default_time)
        time_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
        time_dialog.open()

    def on_save(self, instance, time):
        """ Click OK """
        self.root.ids.time_label.text = str(time)

    def on_cancel(self, instance, time):
        """ Click Cancel """
        self.root.ids.time_label.text = 'You clicked cancel'


MainApp().run()
