from kivy.lang import Builder
from kivymd.app import MDApp

# Image swiper


class MainApp(MDApp):

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('image_swiper.kv')


MainApp().run()
