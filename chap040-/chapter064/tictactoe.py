from kivy.lang import Builder
from kivymd.app import MDApp
from kivy.uix.button import Button


class MainApp(MDApp):
    # Define whose turn it is
    turn = 'X'
    # Keep track of when win or lose
    winner = False
    # Colors
    RESET_COLOR = 'green'
    END_GAME_COLOR = 'red'

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        return Builder.load_file('tictactoe.kv')

    def win(self):
        """Determine who wins the game"""
        # Across
        if self.root.ids.button1.text != '' and self.root.ids.button1.text == self.root.ids.button2.text and self.root.ids.button2.text == self.root.ids.button3.text:
            self.endgame(self.root.ids.button1,
                         self.root.ids.button2, self.root.ids.button3)
        if self.root.ids.button4.text != '' and self.root.ids.button4.text == self.root.ids.button5.text and self.root.ids.button5.text == self.root.ids.button6.text:
            self.endgame(self.root.ids.button4,
                         self.root.ids.button5, self.root.ids.button6)
        if self.root.ids.button7.text != '' and self.root.ids.button7.text == self.root.ids.button8.text and self.root.ids.button8.text == self.root.ids.button9.text:
            self.endgame(self.root.ids.button7,
                         self.root.ids.button8, self.root.ids.button9)

        # Down
        if self.root.ids.button1.text != '' and self.root.ids.button1.text == self.root.ids.button4.text and self.root.ids.button4.text == self.root.ids.button7.text:
            self.endgame(self.root.ids.button1,
                         self.root.ids.button4, self.root.ids.button7)
        if self.root.ids.button2.text != '' and self.root.ids.button2.text == self.root.ids.button5.text and self.root.ids.button5.text == self.root.ids.button8.text:
            self.endgame(self.root.ids.button2,
                         self.root.ids.button5, self.root.ids.button8)
        if self.root.ids.button3.text != '' and self.root.ids.button3.text == self.root.ids.button6.text and self.root.ids.button6.text == self.root.ids.button9.text:
            self.endgame(self.root.ids.button3,
                         self.root.ids.button6, self.root.ids.button9)

        # Diagonal
        if self.root.ids.button1.text != '' and self.root.ids.button1.text == self.root.ids.button5.text and self.root.ids.button5.text == self.root.ids.button9.text:
            self.endgame(self.root.ids.button1,
                         self.root.ids.button5, self.root.ids.button9)
        if self.root.ids.button7.text != '' and self.root.ids.button7.text == self.root.ids.button5.text and self.root.ids.button5.text == self.root.ids.button3.text:
            self.endgame(self.root.ids.button3,
                         self.root.ids.button5, self.root.ids.button7)
        self.no_winner()

    def no_winner(self):
        """Actions when there are no winners"""
        if (self.winner == False and
            self.root.ids.button1.disabled == True and
            self.root.ids.button2.disabled == True and
            self.root.ids.button3.disabled == True and
            self.root.ids.button4.disabled == True and
            self.root.ids.button5.disabled == True and
            self.root.ids.button6.disabled == True and
            self.root.ids.button7.disabled == True and
            self.root.ids.button8.disabled == True and
                self.root.ids['button9'].disabled == True):
            self.root.ids.score.text = "IT'S A TIE!!"

    def endgame(self, btn1: Button, btn2: Button, btn3: Button):
        """End the game"""
        self.winner = True
        btn1.color = self.END_GAME_COLOR
        btn2.color = self.END_GAME_COLOR
        btn3.color = self.END_GAME_COLOR

        # Disable all the buttons
        self.disable_all_buttons()

        # Set label for winner
        self.root.ids.score.text = f'{btn1.text} Wins!'

    def disable_all_buttons(self):
        """Disable all buttons on the board"""
        self.root.ids.button1.disabled = True
        self.root.ids.button2.disabled = True
        self.root.ids.button3.disabled = True
        self.root.ids.button4.disabled = True
        self.root.ids.button5.disabled = True
        self.root.ids.button6.disabled = True
        self.root.ids.button7.disabled = True
        self.root.ids.button8.disabled = True
        self.root.ids['button9'].disabled = True

    def presser(self, button: Button):
        """Action on the press of one of the game board button"""
        if self.turn == 'X':
            button.text = 'X'
            button.disabled = True
            self.root.ids.score.text = "O's Turn!"
            self.turn = 'O'
        else:
            button.text = 'O'
            button.disabled = True
            self.root.ids.score.text = "X's Turn!"
            self.turn = 'X'
        self.win()

    def restart(self):
        """Restart the game"""
        # Reset who's turn it is
        self.turn = 'X'

        # Enable the buttons
        self.root.ids.button1.disabled = False
        self.root.ids.button2.disabled = False
        self.root.ids.button3.disabled = False
        self.root.ids.button4.disabled = False
        self.root.ids.button5.disabled = False
        self.root.ids.button6.disabled = False
        self.root.ids.button7.disabled = False
        self.root.ids.button8.disabled = False
        self.root.ids['button9'].disabled = False

        # Clear the buttons
        self.root.ids.button1.text = ''
        self.root.ids.button2.text = ''
        self.root.ids.button3.text = ''
        self.root.ids.button4.text = ''
        self.root.ids.button5.text = ''
        self.root.ids.button6.text = ''
        self.root.ids.button7.text = ''
        self.root.ids.button8.text = ''
        self.root.ids['button9'].text = ''

        # Rest the buttons's colors
        self.root.ids.button1.color = self.RESET_COLOR
        self.root.ids.button2.color = self.RESET_COLOR
        self.root.ids.button3.color = self.RESET_COLOR
        self.root.ids.button4.color = self.RESET_COLOR
        self.root.ids.button5.color = self.RESET_COLOR
        self.root.ids.button6.color = self.RESET_COLOR
        self.root.ids.button7.color = self.RESET_COLOR
        self.root.ids.button8.color = self.RESET_COLOR
        self.root.ids['button9'].color = self.RESET_COLOR

        # Reset the score label
        self.root.ids['text'] = 'X GOES FIRST!'

        # Reset the winner variable to no winner
        self.winner = False


if __name__ == '__main__':
    MainApp().run()
