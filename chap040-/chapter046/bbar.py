from kivy.lang import Builder
from kivymd.app import MDApp

# Material Design Iconic Font
# Icons cheatsheet
# https://zavoloklom.github.io/material-design-iconic-font/icons.html


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('bbar.kv')


MainApp().run()
