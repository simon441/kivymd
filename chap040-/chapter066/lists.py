from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.list.list import OneLineListItem, ThreeLineListItem, TwoLineListItem


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'
        return Builder.load_file('lists.kv')

    def presser(self, pressed: OneLineListItem, list_id: str):
        pressed.text = f'You pressed {list_id}'

    def presser2(self, pressed: TwoLineListItem, list_id: str):
        pressed.secondary_text = f'You pressed {list_id}'

    def presser3(self, pressed: ThreeLineListItem, list_id: str):
        pressed.tertiary_text = f'You pressed {list_id}'


MainApp().run()
