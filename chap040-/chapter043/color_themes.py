from kivy.lang import Builder
from kivymd.app import MDApp

class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'DeepPurple' # for dark theme
        # self.theme_cls.primary_palette = 'Blue' # for light theme
        self.theme_cls.accent_palette = 'Red'
        return Builder.load_file('color_themes.kv')
    
    
# kivymd.color_definitions.palette = ['Red', 'Pink', 'Purple', 'DeepPurple', 'Indigo', 'Blue', 'LightBlue', 'Cyan', 'Teal', 'Green', 'LightGreen', 'Lime', 'Yellow', 'Amber', 'Orange', 'DeepOrange', 'Brown', 'Gray', 'BlueGray']
#     Valid values for color palette selecting.
# https://kivymd.readthedocs.io/en/latest/themes/color-definitions/#

MainApp().run()
