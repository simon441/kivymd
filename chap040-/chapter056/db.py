from kivy.lang import Builder
from kivymd.app import MDApp
import mysql.connector


DATABASE_NAME = 'kivy_second_db'

# Sqlite Database Management


class MainApp(MDApp):
    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'

        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = mysql.connector.connect(
            host='localhost',
            user='root',
            passwd='root',
            database=DATABASE_NAME,
        )

        # Create a Cursor
        cur = conn.cursor()

        # Create an actual database
        cur.execute(f'CREATE DATABASE IF NOT EXISTS {DATABASE_NAME};')

        # # Check to see if database was created
        # cur.execute('SHOW DATABASES;')
        # # Show current databases
        # for db in cur:
        #     print(db)

        # Create a Table
        cur.execute("""
        CREATE TABLE IF NOT EXISTS customers (
            name VARCHAR(50)
            )
        """)

        # # Check for table existance
        # cur.execute('SELECT * FROM customers')
        # print(cur.description)

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

        return Builder.load_file('first_db.kv')

    def submit(self):
        """ Insert data into database """

        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = mysql.connector.connect(
            host='localhost',
            user='root',
            passwd='root',
            database=DATABASE_NAME,
        )

        # Create a Cursor
        cur = conn.cursor()

        # Add a record
        sql_command = "INSERT INTO customers (name) VALUES(%s)"
        values = (self.root.ids.word_input.text.strip(),)

        # Execute SQL Comand
        cur.execute(sql_command, values)

        # Add a message
        self.root.ids.word_label.text = f'{self.root.ids.word_input.text.strip()} Added'

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()

    def show_records(self):
        """ Show database records """
        # Create Database or Connect to One if it exists

        # Define Database connection details
        conn = mysql.connector.connect(
            host='localhost',
            user='root',
            passwd='root',
            database=DATABASE_NAME,
        )

        # Create a Cursor
        cur = conn.cursor()

        # Grab records from database
        cur.execute("SELECT * FROM customers;")
        records = cur.fetchall()

        word = ''
        # Loop through records
        for record in records:
            word = f'{word} \n{record[0]}'

        # Show database data
        self.root.ids.word_label.text = word

        # Clear the input box
        self.root.ids.word_input.text = ''

        # Commit the changes
        conn.commit()

        # Close the connection
        conn.close()


MainApp().run()
