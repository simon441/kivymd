from kivy.lang import Builder
from kivymd.app import MDApp
from kivymd.uix.picker import MDDatePicker

# Date picker


class MainApp(MDApp):

    def build(self):
        self.theme_cls.theme_style = 'Light'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('datepicker.kv')

    def show_date_picker(self):
        # date_dialog = MDDatePicker(year=2000, month=2, day=13) # default date
        date_dialog = MDDatePicker(mode='range')
        date_dialog.bind(on_save=self.on_save, on_cancel=self.on_cancel)
        date_dialog.open()

    def on_save(self, instance, value, date_range: list):
        """ Click OK """
        self.root.ids.date_label.text = str(value)
        self.root.ids.date_label.text = f'You selected {date_range[0]} to {date_range[len(date_range)-1]}'

    def on_cancel(self, instance, value):
        """ Click Cancel """
        self.root.ids.date_label.text = 'You clicked cancel'


MainApp().run()
