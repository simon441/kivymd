from kivy.lang import Builder
from kivymd.app import MDApp

# Material Icons cheatsheet
# https://materialdesignicons.com/


class MainApp(MDApp):
    data = {
        'language-python': 'Python',
        'language-ruby': 'Ruby',
        'language-javascript': 'JS'
    }

    def build(self):
        self.theme_cls.theme_style = 'Dark'
        self.theme_cls.primary_palette = 'BlueGray'  # for dark theme
        return Builder.load_file('speed_dial.kv')

    def callback(self, instance):
        if instance.icon == 'language-python':
            lang = 'Python'
        elif instance.icon == 'language-ruby':
            lang = 'Ruby'
        elif instance.icon == 'language-javascript':
            lang = 'JS'

        self.root.ids.my_label.text = f'You pressed {lang}'

    def open(self):
        """ Open the speed dial """
        self.root.ids.my_label.text = 'Opened!'

    def close(self):
        """ Close the speed dial """
        self.root.ids.my_label.text = 'Closed!'


MainApp().run()
