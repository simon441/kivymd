import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.core.window import Window
from kivy.core.spelling import Spelling

# Designate the .kv design file
Builder.load_file('spellchecker.kv')


class MyLayout(Widget):
    def press(self):
        """
        on submit
        """
        # Create instance of Spelling
        s = Spelling()

        # Select the language
        s.select_language('en_US')

        # See the language eoptions
        # print(s.list_languages())

        # Grab the word from the textbox
        word = self.ids.word_input.text

        options = s.suggest(word)

        x = ''
        for item in options:
            x = f'{x} {item}'

        # Update the label
        self.ids.word_label.text = f'Suggestions: {x}'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
