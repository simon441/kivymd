from kivy.app import App
from kivy.uix.widget import Widget
from kivy.lang.builder import Builder
from kivy.uix.screenmanager import ScreenManager, Screen

# Define the different screens


class FirstWindow(Screen):
    pass


class SecondWindow(Screen):
    pass


class WindowManager(ScreenManager):
    pass


# Designate the .kv design file
kv = Builder.load_file('new_window.kv')


class MyApp(App):
    def build(self):
        return kv


if __name__ == "__main__":
    MyApp().run()
