from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.animation import Animation

# Designate the .kv design file
Builder.load_file('animations.kv')


class MyLayout(Widget):
    def animate_it(self, widget, *args):
        """ Animate on button click """

        # Define thre animation you want to do
        animate = Animation(
            background_color=(0, 0, 1, 1),
            duration=1,
            #
        )

        # Do second animation
        animate += Animation(size_hint=(1, 1))

        # Do third animation
        animate += Animation(size_hint=(.5, .5))

        # Do fourth animation
        animate += Animation(pos_hint={'center_x': -.1})
        animate += Animation(pos_hint={'center_x': .5})

        # Do last animation
        animate += Animation(opacity=0  # animation duration in seconds
                             , duration=.5)

        # Start The Animation
        animate.start(widget)

        # Create a callback
        animate.bind(on_complete=self.my_callback)

    def my_callback(self, *args):
        self.ids.my_label.text = 'Wow! Look at That!'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
