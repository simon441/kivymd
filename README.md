# Kivy Playlist by Codemy.com

1. Intro To Kivy - Installing Kivy on Windows - Python Kivy GUI Tutorial #1 [https://youtu.be/dLgquj0c5_U]
2. Input Boxes and Buttons - Python Kivy GUI Tutorial #2 [https://youtu.be/S41RPtdWe78]
3. Button Column Span Trick! - Python Kivy GUI Tutorial #3 [https://youtu.be/TVnUxyEUVw0]
4. How To Set The Height And Width of Widgets - Python Kivy GUI Tutorial #4 [https://youtu.be/AxKksRhcmOA]
5. Kivy Design Language - Python Kivy GUI Tutorial #5 [https://youtu.be/k4QCoS-hj-s]
6. The Kivy Builder - Python Kivy GUI Tutorial #6 [https://youtu.be/dVVPOPuPPc0]
7. Changing Kivy Button Colors - Python Kivy GUI Tutorial #7 [https://youtu.be/2IuAQ1HUpU4]
8. Kivy Box Layout - Python Kivy GUI Tutorial #8 [https://youtu.be/ZFGAz6vZx1E]
9. Setting Default Widget Properties - Python Kivy GUI Tutorial #9 [https://youtu.be/e73K1DoTNio]
10. Change Background Color And Text Color of Labels - Python Kivy GUI Tutorial #10 [https://youtu.be/Gt0_BuJmJeI]
11. Two Ways To Change Background Colors - Python Kivy GUI Tutorial #11 [https://youtu.be/KTpYXX1-6yY]
12. How To Use Images With Kivy - Python Kivy GUI Tutorial #12 [https://youtu.be/LMgLt70kAro]
13. Kivy Float Layout - Python Kivy GUI Tutorial #13 [https://youtu.be/9DaVDNP-NOM]
14. How To Update Labels - Python Kivy GUI Tutorial #14 [https://youtu.be/7Sks1Ld1DWY]
15. Build A Simple Calculator App - Python Kivy GUI Tutorial #15 [https://youtu.be/Lu-HP4eOYM4]
16. Calculator Addition Function - Python Kivy GUI Tutorial #16 [https://youtu.be/pdQ_KZS_GRQ]
17. Secondary Calculator Button Functions - Python Kivy GUI Tutorial #17 [https://youtu.be/yzcGh1R6Qes]
18. Fix Our Decimal Calculator Problem - Python Kivy GUI Tutorial #18 [https://youtu.be/AvbcELqNbx0]
19. Math Calculator Buttons With eval() - Python Kivy GUI Tutorial #19 [https://youtu.be/xeXCrZrJazI]
20. Standalone Python EXE Executable - Python Kivy GUI Tutorial #20 [https://youtu.be/NEko7jWYKiE]
    - Step One: pip install pyinstaller
    - Step Two: pyinstaller calculator.py -w
    - Step Three: modify our calculator.spec file, making several changes shown in the video.
    - Step Four: pyinstaller calculator.spec -y
    - Step Five: Zip up all the files into one .zip file that you can share!
    - to build the project from the file do step four
21. Kivy 2.0 Released! How To Install - Python Kivy GUI Tutorial #21 [https://youtu.be/_J8GD-rHbrA]
22. Rounded Buttons With Kivy - Python Kivy GUI Tutorial #22 [https://youtu.be/gQRcY2y3mkY]
23. Image Viewer With FileChooserIconView and FileChooserListView - Python Kivy GUI Tutorial #23 [https://youtu.be/YlRd4rw_vBw]
24. Spell Checker With Kivy - Python Kivy GUI Tutorial #24 [https://youtu.be/NjUV_QxWNEQ]
25. Sliders For Kivy - Python Kivy GUI Tutorial #25 [https://youtu.be/18bvQW2OHZE]
26. Accordions For Kivy - Python Kivy GUI Tutorial #26 [https://youtu.be/h_5QsY_Es00]
27. Carousels For Kivy - Python Kivy GUI Tutorial #27 [https://youtu.be/OP7JpXGzoZE]
28. CheckBoxes For Kivy - Python Kivy GUI Tutorial #28 [https://youtu.be/RpAzki0UJPI]
29. Radio Buttons For Kivy - Python Kivy GUI Tutorial #29 [https://youtu.be/X-9l-Sll_gE]
30. Popup Boxes For Kivy - Python Kivy GUI Tutorial #30 [https://youtu.be/NzUTZj31AfM]
31. Multiple Windows With ScreenManager - Python Kivy GUI Tutorial #31 [https://youtu.be/Prt_SKkZji8]
    - import Screen and ScreenManager
    - create a class per Screen and a class inheriting from the ScreenManager
32. Spinner Dropdowns - Python Kivy GUI Tutorial #32 [https://youtu.be/Wu7kTFZtM6I]
33. Resize Widgets With Splitters - Python Kivy GUI Tutorial #33 [https://youtu.be/jOaIL8HiO6U]
    - Splitters: put next widget for resize inside the Splitter
34. Tabs For Kivy - Python Kivy GUI Tutorial #34 [https://youtu.be/hoEbMTE_k-M]
35. Using Images As Buttons - Python Kivy GUI Tutorial #35 [https://youtu.be/mEAjGChhwcs]
36. How To Create Animation With Kivy - Python Kivy GUI Tutorial #36 [https://youtu.be/1fTx2oKJMOQ]
37. How To Create Progress Bars With Kivy - Python Kivy GUI Tutorial #37 [https://youtu.be/D1Lg3oR_qig]
38. How To Use Markup To Change Text Style - Python Kivy GUI Tutorial #38
39. How To Create a Switch With Kivy - Python Kivy GUI Tutorial #39 [https://youtu.be/4-PASskUCW0]
40. Intro To KivyMD Installation - Python Kivy GUI Tutorial #40 [https://youtu.be/ycoKlFV3-iU]
41. How To Teach Yourself KivyMD Quickly - Python Kivy GUI Tutorial #41
42. Which is Better Kivy Or Tkinter? - Python Kivy GUI Tutorial #42
43. Using Color Themes For KivyMD - Python Kivy GUI Tutorial #43
44. Creating A Login Screen With KivyMD - Python Kivy GUI Tutorial #44
45. Create A Bottom Bar Button with KivyMD - Python Kivy GUI Tutorial #45
46. Navbar With Icons with KivyMD - Python Kivy GUI Tutorial #46
47. Speed Dial Button Menu With KivyMD - Python Kivy GUI Tutorial #47
48. Alert Dialog Boxes For KivyMD - Python Kivy GUI Tutorial #48
49. Build An Image Swiper App For KivyMD - Python Kivy GUI Tutorial #49
50. Image Swiper App Tricks and Tips - Python Kivy GUI Tutorial #50
51. KivyMD Date Picker - Python Kivy GUI Tutorial #51
52. KivyMD Time Picker - Python Kivy GUI Tutorial #52
53. KivyMD DataTables - Python Kivy GUI Tutorial #53
54. KivyMD Pagination For Data Tables - Python Kivy GUI Tutorial #54
55. Using SQLite3 Database With Kivy - Python Kivy GUI Tutorial #55
56. Using MySQL Database With Kivy - Python Kivy GUI Tutorial #56
57. Using Postgres Database With Kivy - Python Kivy GUI Tutorial #57
58. Remove Titlebar From Kivy App - Python Kivy GUI Tutorial #58
59. Add Matplotlib Graph To Kivy - Python Kivy GUI Tutorial #59
60. Add A Map To Your Kivy App - Python Kivy GUI Tutorial #60
61. Using Python Code In a .kv Design File - Python Kivy GUI Tutorial #61
62. Add Keyboard With VKeyboard - Python Kivy GUI Tutorial #62
63. Build A Tic Tac Toe Game Part 1 - Python Kivy GUI Tutorial #63
64. Build A Tic Tac Toe Game Logic Part 2 - Python Kivy GUI Tutorial #64
65. Build A Tic Tac Toe Game Part 3 Keep Score - Python Kivy GUI Tutorial #65
66. Lists With KivyMD - Python Kivy GUI Tutorial #66
67. Video Player With Kivy - Python Kivy GUI Tutorial #67

## Usage

1. For the first chapters 1 to 39:
    - create a virtual env:  ```python -m venv ./venv```
    - activate the env: Linux & OSX:```source ./venv/Scripts/activate```, Windows: powershell: ```.\venv\Scripts\activate.ps1``` or on command line: ```venv\Scripts\activate.bat```
    - install the dependencies: ```pip install -r requirements.txt```

2. For the chapters 40 and after
   - navigate to chap040- : ```cd chap040-```
   - unzip KivyMD2.zip files in a directory called  "KivyMD2"
   - update the LivyMD2 line in the requirements.txt file to match the absolute path of the KivyMD2 folder (eg: file:///C:/Kivy/KivyMD2)
   - do the same steps as for the chapter 1 to 39.
