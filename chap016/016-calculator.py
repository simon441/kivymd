import kivy
from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.core.window import Window

# Set the app size
Window.size = (500, 700)

# Designate the .kv design file
# Builder.load_file('calculator.kv')


class MyLayout(Widget):
    def clear(self):
        """
        Clear the Calculator text
        """
        self.ids.calc_input.text = '0'

    def button_press(self, button):
        """
        Button pressing function
        """
        # Create a variable that contains whatever was already in the textbox
        prior = self.ids.calc_input.text

        # determine if 0 is sitting there
        if prior == "0":
            self.ids.calc_input.text = ''
            self.ids.calc_input.text = f'{button}'
        else:
            self.ids.calc_input.text = f'{prior}{button}'

    def add(self):
        """
        Addition function
        """
        prior = self.ids.calc_input.text
        # slap a plus sign to the text box
        self.ids.calc_input.text = f'{prior}+'

    def substract(self):
        """
        Substraction function
        """
        prior = self.ids.calc_input.text
        # slap a minus sign to the text box
        self.ids.calc_input.text = f'{prior}-'

    def multiply(self):
        """
        Multiplication function
        """
        prior = self.ids.calc_input.text
        # slap a multiply sign to the text box
        self.ids.calc_input.text = f'{prior}x'

    def divide(self):
        """
        Division function
        """
        prior = self.ids.calc_input.text
        # slap a division sign to the text box
        self.ids.calc_input.text = f'{prior}÷'

    def equals(self):
        """
        Compute result
        """
        prior = self.ids.calc_input.text

        # Adition
        if '+' in prior:
            num_list = prior.split('+')
            answer = 0
            for number in num_list:
                answer += int(number)
            # print the answer in the text box
            self.ids.calc_input.text = str(answer)


class CalculatorApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    CalculatorApp().run()
