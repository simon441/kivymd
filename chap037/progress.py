from kivy.app import App
from kivy.uix.widget import Widget
from kivy.properties import ObjectProperty
from kivy.lang.builder import Builder
from kivy.animation import Animation

# Designate the .kv design file
Builder.load_file('progress.kv')


class MyLayout(Widget):
    def press_it(self):
        # Grab the current progress bar value
        current = self.ids.my_progress_bar.value
        # If statement to start over after value reaches 100
        if current == 1:
            current = 0
        # Increament value by .25
        current += .25
        # Update the progress bar
        self.ids.my_progress_bar.value = current
        # Update the label
        self.ids.my_label.text = f'{current*100}% Progress'


class MyApp(App):
    def build(self):
        return MyLayout()


if __name__ == "__main__":
    MyApp().run()
